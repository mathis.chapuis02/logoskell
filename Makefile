SRC_DIR=src
OUTPUT_DIR=output

all: build-folder build

build-folder:
	mkdir -p $(OUTPUT_DIR)

build:
	ghc $(SRC_DIR)/main.hs -o $(OUTPUT_DIR)/buildSVG
