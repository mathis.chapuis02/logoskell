# LOGOSKELL

This project was made during an Haskell module made at the INSA Lyon Dpt. Telecom.

To compile the project, you must have the `ghc` -Glasgow Haskell Compiler- installed to compile the main. Please go on the [official website](https://www.haskell.org/) for more informations.

## Compile the project

A Makefile was created to compile this project. Run the following command to compile into an executable:

```
make
```

The compiled file will be under the `output` directory.

## Convert LOGOSKELL instructions into an SVG

Logoskell supports 4 instructions:

* `Forward x` : makes the pen move and draw in the current direction.
* `Right x` : makes the pen rotate clockwise
* `Left x` : makes the pen rotate anticlockwise
* `Repeat i [instructions]` : repeats `i` times the list of `instructions` provided

When you run the executable, the program expects a list of instructions on the standard input (unix `<` redirection works). The list must be between brackets (`[]`), and the all instructions must be separated by commas (`,`). You may see an example below:

```
[ Forward 10, Left 20, Repeat 4 [ Forward 10, Right 15 ] ]
```

The corresponding SVG will be printed on the standard output.

At this moment, it is not possible to change the size of the SVG, nor the color.
