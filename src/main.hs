import Prelude hiding (Left, Right)
import System.Exit ( exitSuccess )

-- Definition of the instructions recognized as LOGOSKELL's instruction
data Instruction = Forward Float
    | Left Float
    | Right Float
    | Repeat Int [Instruction]
    deriving (Show, Read)

-- List of Instructions
type InstructionList = [Instruction]

-- Pen having x / y coordinates, and angle
data Pen = Pen Float Float Float deriving (Show)

-- Point having x and y coordinates. Used to draw a segment in the SVG
data Point = Point {x :: Float, y :: Float} deriving (Show)

-- Function to parse a string and create a list of instructions
parse :: String -> InstructionList
parse string = read string :: InstructionList

-- Function to add n times a list of instructions (see the Repeat Instruction)
addRepeatToList :: Int -> InstructionList -> InstructionList -> InstructionList
addRepeatToList n x list
    | n == 0 = list
    | n > 0 = addRepeatToList (n-1) x (x++list)

-- Function returning an array of points using a list of LOGOSKELL Instructions and a pen
logoskellToPoints :: InstructionList -> Pen -> [Point] -> [Point]
-- Return the points when there are no instructions
logoskellToPoints [] _ points = points
logoskellToPoints (current:rest) (Pen x y angle) points = case current of 
    Forward v -> if null points
        then logoskellToPoints rest newPen [newPoint, Point x y]
        else logoskellToPoints rest newPen (newPoint:points)
        where newPen = Pen (x + v*cos(angle*pi/180)) (y + v*(-sin(angle*pi/180))) angle
              newPoint = Point (x + v*cos(angle*pi/180)) (y + v*(-sin(angle*pi/180)))
    Left v -> logoskellToPoints rest newPen points
        where newPen = Pen x y (angle - v)
    Right v -> logoskellToPoints rest newPen points
        where newPen = Pen x y (angle + v)
    Repeat i repeated -> logoskellToPoints (addRepeatToList i repeated [] ++ rest) (Pen x y angle) points


-- Transform Points into svg tags
pointsToSvg :: [Point] -> [String] -> [String]
pointsToSvg (Point {x = x1, y = y1}:p2@Point {x = x2, y = y2}:rest) svgContent =
    pointsToSvg (p2:rest) newSvgContent
        where newSvgContent = svgContent++["<line x1=\"" ++ show x1 ++ "\" y1=\"" ++ show y1 ++ "\" x2=\"" ++ show x2 ++ "\" y2=\"" ++ show y2 ++ "\" stroke=\"red\" stroke-width=\"1\"/>"]
pointsToSvg [_] svgContent = svgContent++["</svg>"]
pointsToSvg [] svgContent = svgContent

-- Function to output the list of Strings representing the SVG
outputSvg :: [String] -> IO()
outputSvg [] = return ()
outputSvg (x:xs) = do
    putStrLn x
    outputSvg xs

main :: IO()
main = do
    -- Define the origin of the pen
    let pen = Pen 200.0 200.0 0
    -- Define svg header
    let svg = ["<?xml version=\"1.0\" encoding=\"utf-8\"?>","<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"400\" height=\"400\">","<title>Test</title>"]

    -- Get the instruction list from stdin (either the user type it or make a redirection)
    content <- getLine

    -- Output logoskell instructions processing
    outputSvg (pointsToSvg (logoskellToPoints (parse content) pen []) svg)
    exitSuccess
